﻿namespace ImageManipulationTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.obrazek = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.getimgButton = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.obrazek)).BeginInit();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(197, 227);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "convert";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // obrazek
            // 
            this.obrazek.Location = new System.Drawing.Point(0, 0);
            this.obrazek.Name = "obrazek";
            this.obrazek.Size = new System.Drawing.Size(140, 90);
            this.obrazek.TabIndex = 1;
            this.obrazek.TabStop = false;
            this.obrazek.Click += new System.EventHandler(this.obrazek_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // getimgButton
            // 
            this.getimgButton.Location = new System.Drawing.Point(157, 12);
            this.getimgButton.Name = "getimgButton";
            this.getimgButton.Size = new System.Drawing.Size(102, 23);
            this.getimgButton.TabIndex = 2;
            this.getimgButton.Text = "choose image";
            this.getimgButton.UseVisualStyleBackColor = true;
            this.getimgButton.Click += new System.EventHandler(this.getimgButton_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.getimgButton);
            this.Controls.Add(this.obrazek);
            this.Controls.Add(this.button1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.obrazek)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.PictureBox obrazek;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button getimgButton;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ImageManipulationTest
{
    public partial class Form1 : Form
    {
        private BigImageForm form;
        public Form1()
        {
            InitializeComponent();
        }

        private void getimgButton_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = ImageDatas.GetImageFilter();
            var res = openFileDialog1.ShowDialog();
            
            if (res == DialogResult.OK)
            {
                obrazek.ImageLocation = openFileDialog1.FileName;
            }
        }

        private void obrazek_Click(object sender, EventArgs e)
        {
            ShowBigImage(obrazek);
        }

        private void ShowBigImage(PictureBox pictureBox)
        {
            form = new BigImageForm();
        
                PictureBox obr = new PictureBox
                {
                    Anchor = AnchorStyles.None,
                    ImageLocation = pictureBox.ImageLocation
                };
                form.Text = "Big one";
                obr.Width = pictureBox.Image.Width;
                obr.Height = pictureBox.Image.Height;
                obr.Left = (this.ClientSize.Width - obr.Width) / 2;
                obr.Top = (this.ClientSize.Height - obr.Height) / 2;
                form.panel1.Controls.Add(obr);
                form.Width = pictureBox.Image.Width + 50;
                form.Height = pictureBox.Image.Height + 50;
                
                form.Show();
           

        }

    }
}
